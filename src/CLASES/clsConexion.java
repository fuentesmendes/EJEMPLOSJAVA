
package CLASES;

import java.awt.HeadlessException;
import java.sql.*;
import javax.swing.*;

public class clsConexion {
    //Variables de conexion
    private static String servidor = "jdbc:mysql://localhost/pruebas";
    private static String user = "root"; //Usuario de la base de datos
    private static String pass = "123456"; //Contraseña de la base de datos
    private static String driver = "com.mysql.jdbc.Driver"; //Driver de conexion con la base MySql
    private static Connection conectar; //Variable de conexion de tipo Connection
    
    public clsConexion()
    {
        try {
            Class.forName(driver);
            conectar = DriverManager.getConnection(servidor, user, pass);
//            JOptionPane.showMessageDialog(null,"CONEXION EXITOSA");
        } catch (HeadlessException | ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null,"CONEXION ERRADA");
        }
    }
    
    public Connection DevuelveConexion()
    {
        return conectar;
    }
    
}
